module.exports = {
    ci: {
        collect: {
            startServerCommand: "npm run start",
            numberOfRuns: 1,
            url: [
                "https://mirumee.com",
            ],
        },
        upload: {
            target: "lhci",
            serverBaseUrl: "https://bref-chocolatine-35176.herokuapp.com",
            token: "9ed66d03-761b-4956-8586-3c769f68b04b",
            ignoreDuplicateBuildFailure: true,
        },
    },
};


//
// module.exports = {
//     ci: {
//         collect: {
//             settings: {chromeFlags: '--no-sandbox'},
//         },
//         upload: {
//             target: 'temporary-public-storage',
//         },
//     },
// };